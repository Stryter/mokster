using System;
using Moq;
using Xunit;

namespace Mokster.Tests
{
    public class MyMockableClass {
        public virtual string DoItAll() {
            return "It is all done";
        }
    }
    public class UnitTest1 : MoksterFixture
    {
        [Fact]
        public void Test1()
        {
            ForMock<MyMockableClass>(mock => {
                mock.Setup(myMockable => myMockable.DoItAll()).ResultF("My Result");
            });

            MyMockableClass myClass = ForMocked<MyMockableClass>();
            myClass.DoItAll();

            ForMock<MyMockableClass>(mock => {
                mock.Verify(myMockable => myMockable.DoItAll(), Times.Once());
            });
            Console.WriteLine("I made it!");
        }
    }
}
