﻿using System;
using System.Collections.Generic;
using Moq;

namespace Mokster
{
    public abstract class MoksterFixture : IMocksterApi<MoksterFixture>
    {
        private IMoksterConfigurer _moksterConfigurer;

        public MoksterFixture()
        {
            _moksterConfigurer = new MoksterConfigurer();    
        }

        public MoksterFixture ForMock<T>(Action<Mock<T>> configureMockDelegate = null, string mockReferenceName = null) where T : class
        {
            _moksterConfigurer.ForMock(configureMockDelegate);
            return this;
        }

        public T ForMocked<T>(string mockReferenceName = null) where T : class
        {
            return _moksterConfigurer.ForMocked<T>(mockReferenceName);
        }

        public Mock<T> Mock<T>(string mockReferenceName = null) where T : class
        {
            return _moksterConfigurer.Mock<T>(mockReferenceName);
        }
    }
}
