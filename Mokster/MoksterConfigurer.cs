using System;
using System.Collections.Generic;
using Moq;

namespace Mokster {
    public interface IMoksterConfigurer : IMocksterApi<IMoksterConfigurer> { }

    class MoksterConfigurer : IMoksterConfigurer
    {
        private IDictionary<string, object> _trackedObjects = new Dictionary<string, object>();
        
        public IMoksterConfigurer ForMock<T>(Action<Mock<T>> configureMockDelegate = null, string mockReferenceName = null) where T : class
        {
            Mock<T> mockObj = Mock<T>(mockReferenceName);
            
            configureMockDelegate?.Invoke(mockObj);
            
            return this;
        }

        public T ForMocked<T>(string mockReferenceName = null) where T : class
        {
            return Mock<T>(mockReferenceName).Object;
        }

        public Mock<T> Mock<T>(string mockReferenceName = null) where T : class
        {
            Type typeToMock = typeof(T);

            string referenceName = GetReferenceNameFromTypeAndOverride(typeToMock, mockReferenceName);

            Mock<T> trackedMock = null;

            if (IsTracked(referenceName)) {
                trackedMock = RetrieveTrackedObject(referenceName).AsMock<T>();
            } else {
                trackedMock = ConstructEmptyMockOfType(typeToMock).AsMock<T>();

                TrackObject(referenceName, trackedMock);
            }

            return trackedMock;
        }

        private void TrackObject(string referenceName, Mock trackedMock)
        {
            _trackedObjects.Add(referenceName, trackedMock);
        }

        private object RetrieveTrackedObject(string referenceName)
        {
            if (_trackedObjects.TryGetValue(referenceName, out object trackedObject)) {
                return trackedObject;
            }

            throw new Exception($"Attempted to retrieve an object that is not currently being stored: {referenceName}");
        }

        private bool IsTracked(string referenceName)
        {
            return _trackedObjects.ContainsKey(referenceName);
        }

        private string GetReferenceNameFromTypeAndOverride(Type type, string referenceNameOverride)
        {
            string typeName = type.FullName;
            bool hasOverride = !string.IsNullOrEmpty(referenceNameOverride);

            return $"{typeName}{(hasOverride ? "::" + referenceNameOverride : string.Empty)}";
        }

        private static object ConstructEmptyMockOfType(Type typeToMock) {
            Type typeOfMock = typeof(Mock<>).MakeGenericType(typeToMock);

            return Activator.CreateInstance(typeOfMock);
        }
    }
}