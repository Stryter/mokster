
using System;
using Moq;

namespace Mokster
{
    public interface IMocksterApi<U> where U : class
    {
        U ForMock<T>(Action<Mock<T>> configureMockDelegate = null, string mockReferenceName = null) where T : class;

        T ForMocked<T>(string mockReferenceName = null) where T : class;

        Mock<T> Mock<T>(string mockReferenceName = null) where T : class;
    }
}
