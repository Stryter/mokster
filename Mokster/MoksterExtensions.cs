
using System;
using Moq;
using Moq.Language.Flow;

public static class MoksterExtensions
{
    public static IReturnsResult<T> ResultF<T, TResult>(this ISetup<T, TResult> setup, TResult valueToReturn) where T : class {
        return setup.Returns(() => valueToReturn);
    }

    public static Mock<T> AsMock<T>(this object mockObject) where T : class {
        if (mockObject is Mock) {
            return mockObject as Mock<T>;
        }

        throw new ArgumentException("is not a type of Mock", nameof(mockObject));
    }
}